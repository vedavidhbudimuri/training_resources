Learning Face Recognition:
* https://docs.aws.amazon.com/rekognition/latest/dg/what-is.html
* https://docs.aws.amazon.com/rekognition/latest/dg/faces.html
* https://docs.aws.amazon.com/rekognition/latest/dg/collections.html
* https://docs.aws.amazon.com/rekognition/latest/dg/how-it-works.html


Boto:
https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/rekognition.html



Learning Text To Speech Conversion:
* https://aws.amazon.com/polly/
* https://docs.aws.amazon.com/polly/latest/dg/what-is.html
* https://docs.aws.amazon.com/polly/latest/dg/ssml.html
* https://docs.aws.amazon.com/polly/latest/dg/speechmarks.html

Extra:
* https://docs.aws.amazon.com/polly/latest/dg/NTTS-main.html
* https://docs.aws.amazon.com/polly/latest/dg/managing-lexicons.html

Boto:
https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/polly.html


Learning Language Translation
* https://aws.amazon.com/translate/
* https://docs.aws.amazon.com/translate/latest/dg/what-is.html
* https://docs.aws.amazon.com/translate/latest/dg/how-it-works.html
* https://docs.aws.amazon.com/translate/latest/dg/how-custom-terminology.html

Boto:
https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/translate.html


