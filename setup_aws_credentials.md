
* Create a folder .aws in home folder 
```
mkdir -p ~/.aws
```
* create a file credentials in it 
```
touch ~/.aws/credentials
```
* Open the file with vim editor 
```
vi ~/.aws/credentials
```
* Add the below content with appropriate values 
```
[dev]
aws_access_key_id=<ACCESS KEY ID>
aws_secret_access_key=<ACCESS SECRET KEY>
region=ap-south-1
```